
<?php
require_once('./connexion.php');

function movieByName($filmName){
  if (!empty($filmName)){
    $maj= ucfirst($filmName);
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    $connexion=new PDO($dsn,USER,PASSWD);
    $sql="SELECT *
          from FILM
          where titreOriginalF LIKE '%$filmName%' or titreOriginalF LIKE '%$maj%'";
    $stmt=$connexion->prepare($sql);
    $stmt->execute();
    return $sql;
    }
  }


function movieById($filmId){
  if (!empty($filmId)){
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    $connexion=new PDO($dsn,USER,PASSWD);
    $sql="SELECT titreOriginalF
          from FILM
          where idF=$filmId";
    $stmt=$connexion->prepare($sql);
    $stmt->execute();
    return $sql;
  }
}

function movieByGenre($filmGenre){
  if (!empty($filmGenre)){
    $dsn="mysql:dbname=".BASE.";host=".SERVER;
    $connexion=new PDO($dsn,USER,PASSWD);
    $sql="SELECT distinct titreOriginalF
          from CLASSIFICATION natural join GENRE natural join FILM
          where nomG=$filmGenre";
    $stmt=$connexion->prepare($sql);
    $stmt->execute();
    return $sql;
  }
}
?>
