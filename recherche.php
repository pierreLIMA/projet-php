<html>
  <head>
    <title>
      Connexion à MySQL avec PDO
    </title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="tabstyle.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
    <h1>
      Recherche
    </h1>

    <?php
    require("modele.php");
    require("header.php");
    require_once('connexion.php');
    $connexion=connect_bd();
    if (isset($_GET['search']))
    {
      $sql=movieByName($_GET['search']);
      if(!$connexion->query($sql)) echo "Pb d'accès aux Films";
      else{
        echo "<ul class='list-group'>";
        $cpt=0;
        foreach ($connexion->query($sql) as $row){
          echo "<li class='list-group-item'><a data-toggle='collapse' href='#collapse".$cpt."'>".$row['titreOriginalF']."</a></li>";
          echo "<div id='collapse".$cpt."' class='panel-collapse collapse'>
                  <div class='panel-body'>Titre français : ".$row['titreFrancaisF']."
                  \n<br/>Rélisateur : ".$row['realisateurF']."
                  \n<br/>Pays : ".$row['paysF']."
                  \n<br/>Date de sortie : ".$row['dateF']."
                  \n<br/>Durée du film : ".$row['dureeF']." min
                  \n<br/>Film en ".$row['couleurF']."

                  </div>

                </div>";
          $cpt=$cpt+1;
        }
        echo "</ul>";
      }
    }
    else if (isset($_GET['genre'])){
      $sql=movieByGenre($_GET['genre']);
      if(!$connexion->query($sql)) echo "Pb d'accès aux Films";
      else{
        foreach ($connexion->query($sql) as $row){
          echo $row['titreOriginalF']."<br/>";
        }
        echo "Fin de recherche";
      }
    }
    else if (isset($_GET['id'])){
      $sql=movieById($_GET['id']);
      if(!$connexion->query($sql)) echo "Pb d'accès aux Films";
      else{
        foreach ($connexion->query($sql) as $row){
          echo $row['titreOriginalF']."<br/>";
        }
        echo "Fin de recherche";
      }
    }
    else{
      echo "Pas de recherche";
    }
    ?>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
  </html>
